package com.hcf;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.hcf.vo.Result;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class loginJwtTest {


    @Test
    public void test1() {
        //1.生成map 存放加密的信息
        Map<String, Object> hashMap = new HashMap<>();
        hashMap.put("username", "张三");
        hashMap.put("password", "123456");

        //2.
        String token = Jwts.builder()
                .setClaims(hashMap)//设置想要加密的信息
                //参数1加密方式:加密算法 参数二:秘钥
                .signWith(SignatureAlgorithm.HS256,"")
                //token有限期
                .setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 60 * 24))
                //生成token
                .compact();

        System.out.println(token);

    }

    @Test
    public void test2(){
        Claims claims = Jwts.parser()
                .setSigningKey("hcf")
                .parseClaimsJws("")
                .getBody();
        System.out.println(claims);

    }

    @Test
    public void test3() throws JsonProcessingException {
        //转json工具
        Gson gson = new Gson();
        String json = new ObjectMapper().writeValueAsString(Result.error("NOT_LOGIN"));
        String json1 = gson.toJson(Result.error("NOT_LOGIN"));
        System.out.println("json1 = " + json1);
        //json1 = {"code":0,"msg":"NOT_LOGIN"}
        System.out.println("json = " + json);
        //json = {"code":0,"msg":"NOT_LOGIN","data":null}

    }

}
