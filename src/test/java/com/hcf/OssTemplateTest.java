package com.hcf;


import com.hcf.util.OssTemplate;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

@SpringBootTest
@Slf4j
public class OssTemplateTest {

    @Autowired
    private OssTemplate ossTemplate;

    @Test
    public void testFileUpload() throws FileNotFoundException {
        String filePath = ossTemplate.upload("1.jpg", new FileInputStream("D:\\picture\\1.jpg"));
        log.info("文件上传完毕之后的路径{}", filePath);
    }
}