package com.hcf.aspect;

import com.hcf.anno.LogAnno;
import com.hcf.mapper.OperateLogMapper;
import com.hcf.pojo.OperateLog;
import com.hcf.utils.JwtUtils;
import io.jsonwebtoken.Claims;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.Arrays;

/**
 * AOP记录日志的切面
 */
@Aspect
@Component
public class LogAspect {

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private OperateLogMapper operateLogMapper;

    //切点 标注anno注解的方法都被切中
    @Pointcut("@annotation(com.hcf.anno.LogAnno)")
    public  void  cf(){}


    //环绕通知
    @Around("cf()")
    public Object around(ProceedingJoinPoint pjp) throws Throwable {
        OperateLog operateLog = new OperateLog();
        //获取类名
        operateLog.setClassName(pjp.getTarget().getClass().getName());
        //获取方法参数
        operateLog.setMethodParams(Arrays.toString(pjp.getArgs()));
        //import org.aspectj.lang.reflect.MethodSignature; 获取签名
        MethodSignature signature = (MethodSignature) pjp.getSignature();
        //获取方法名
        operateLog.setMethodName(signature.getMethod().getName());

        LogAnno logAnno = signature.getMethod().getAnnotation(LogAnno.class);
        //获取方法描述
        operateLog.setMethodDesc(logAnno.methodDesc());

        String token = request.getHeader("token");
        Claims claims = JwtUtils.parseJWT(token);
        //拿到用户id
        Integer id = claims.get("id", Integer.class);
        //操作id
        operateLog.setOperateUser(id);
        //操作时间
        operateLog.setOperateTime(LocalDateTime.now());

        long begin = System.currentTimeMillis();
        Object object = pjp.proceed();
        long end = System.currentTimeMillis();

        //方法执行时间
        operateLog.setCostTime(end - begin);

        //方法返回信息
        operateLog.setReturnValue(object.toString());

        //落库
        operateLogMapper.insert(operateLog);
        return object;



    }

}
