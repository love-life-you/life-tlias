package com.hcf.controller;

import com.hcf.anno.LogAnno;
import com.hcf.pojo.Emp;
import com.hcf.vo.PageBean;
import com.hcf.service.EmpService;
import com.hcf.vo.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

/**
 * 员工表示层
 */
@Slf4j
@RestController
@RequestMapping("/emps")
public class EmpController {


    @Autowired
    private EmpService empService;


    /**
     * 分页查询
     *
     * @param page
     * @param pageSize
     * @return
     */
    @GetMapping
    public Result pageEmp(@RequestParam(defaultValue = "1") Integer page,
                          @RequestParam(defaultValue = "10") Integer pageSize,
                          String name,
                          Short gender,
                          @DateTimeFormat(pattern = "YYYY-MM-dd") LocalDate begin,
                          @DateTimeFormat(pattern = "YYYY-MM-dd") LocalDate end) {


        PageBean pageBean = empService.pageEmp(page, pageSize, name, gender, begin, end);

        return Result.success(pageBean);

    }

    /**
     * 根据id删除员工
     *
     * @param
     * @return
     */
    @LogAnno(methodDesc = "根据id删除")
    @DeleteMapping("/{ids}")
    public Result deleteEmp(@PathVariable List<Integer> ids) {
        log.info("根据id批量删除:{}",ids);
        empService.deleteEmp(ids);
        return Result.success();
    }

    /**
     * 新增员工
     * @param emp
     * @return
     */
    @LogAnno(methodDesc = "新增员工")
    @PostMapping
    public Result insertEmp(@RequestBody Emp emp){
        log.info("新增员工:{}",emp);
        empService.insertEmp(emp);
        return Result.success();

    }

    /**
     *根据Id查询数据
     * @param id
     * @return
     */
    @LogAnno(methodDesc = "根据Id查询数据")
    @GetMapping("/{id}")
    public Result selectById(@PathVariable("id") Integer id){
        log.info("根据id查询数据:{}",id);
        //每次只查一个对像,用实体类接收
      Emp emp=  empService.selectById(id);
        return Result.success(emp);

    }

    /**
     * 修改员工
     * @param emp
     * @return
     */
    @LogAnno(methodDesc = "修改员工")
    @PutMapping
    public Result updateEmp(@RequestBody  Emp emp){
        log.info("修改员工:{}",emp);
        empService.updateEmp(emp);
        return Result.success();
    }

}
