package com.hcf.controller;

import com.hcf.pojo.Emp;
import com.hcf.service.EmpService;
import com.hcf.utils.JwtUtils;
import com.hcf.vo.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * 登录
 */
@Slf4j
@RestController
public class LoginController {


    @Autowired
    private EmpService empService;

    @PostMapping("/login")
    public Result login(@RequestBody Emp emp){
        log.info("员工登录:{}",emp);
       Emp e= empService.login(emp);

        //登录成功就生成令牌,返回令牌给前端
       if (e !=null){
           Map<String, Object> claims = new HashMap<>();
           claims.put("id",e.getId());
           claims.put("name",e.getName());
           claims.put("username",e.getUsername());
           //生成token
           String jwt = JwtUtils.generateJwt(claims);
           //返回给前端
           return Result.success(jwt);
       }

       return  Result.error("用户名或密码错误!");
      // return e!=null? Result.success():Result.error("密码或者账号错误");
    }






}
