package com.hcf.controller;

import com.hcf.anno.LogAnno;
import com.hcf.pojo.Dept;
import com.hcf.service.DeptService;
import com.hcf.vo.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 部门表示层
 */
@Slf4j
@RestController
@RequestMapping("/depts")
public class DeptController {

    //靓仔添加了这条注释,

    @Autowired
    private DeptService deptService;

    /**
     * 查询部门数据
     * @return
     */
    @GetMapping
    @LogAnno(methodDesc = "查询部门数据")
    public Result findAllDept() {
        log.info("查询所有部门数据");
        List<Dept> deptList = deptService.findAllDept();


        return Result.success(deptList);

    }

    @DeleteMapping("/{id}")
    @LogAnno(methodDesc = "根据id删除部门")
    public Result deleteByIdDept(@PathVariable Integer id){
        log.info("根据id删除部门:{}",id);
        deptService.deleteById(id);

        return Result.success();
    }

    @PostMapping
    @LogAnno(methodDesc = "添加部门")
    public  Result insertDept(@RequestBody Dept  dept){
        log.info("添加部门:{}",dept);
        deptService.insertDept(dept);
        return Result.success();

    }
    /**
     * 根据id查询回显数据
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    @LogAnno(methodDesc = "根据id查询回显数据")
    public Result selectById(@PathVariable Integer id){
        Dept dept = deptService.selectById(id);
        return Result.success(dept);
    }

    @PutMapping
    @LogAnno(methodDesc = "修改部门数据")
    public Result updateDept(@RequestBody  Dept dept){
        deptService.updateDept(dept);
        return Result.success();

    }


}
