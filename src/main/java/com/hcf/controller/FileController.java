package com.hcf.controller;




import com.hcf.util.OssTemplate;
import com.hcf.vo.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * 图片上传oss
 */
@Slf4j
@RestController
public class FileController {

    @Autowired
    private OssTemplate ossTemplate;

    @PostMapping("/upload")
    public Result uploadFile(MultipartFile image) throws IOException {
        String filePath = ossTemplate.upload(image.getOriginalFilename(), image.getInputStream());
        log.info("文件上传成功,访问地址:{}",filePath);
        return Result.success(filePath);
    }

}
