package com.hcf.anno;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 自定义注解 日志
 */
@Retention(RetentionPolicy.RUNTIME) //保留到运行阶段
@Target({ElementType.METHOD}) //标注在方法上
public @interface LogAnno {

    String methodDesc() default ""; //方法引用
}
