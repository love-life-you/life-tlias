package com.hcf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@EnableAspectJAutoProxy //开启aop
@SpringBootApplication //启动类
@ServletComponentScan //开启过滤器
public class WebStart {
    public static void main(String[] args){
        SpringApplication.run(WebStart.class);
    }
}
