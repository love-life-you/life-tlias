package com.hcf.vo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 分页封装返回实体类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PageBean<T> {

    /**
     * 总记录数
     */
    private Long total;
    /**
     *数据列表
     */
    private List<T> rows;
}
