package com.hcf.Interceptor;

import com.google.gson.Gson;
import com.hcf.utils.JwtUtils;
import com.hcf.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 拦截器校验登录实现代码
 */
@Component
public class LoginInterceptor implements HandlerInterceptor {

    @Autowired
    private Gson gson;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //请求头中拿令牌
        String token = request.getHeader("token");


        //如果字符串没有长度(null 或 空串),返回登录错误
        //import org.springframework.util.StringUtils;
        if (!StringUtils.hasLength(token)){
            String json = gson.toJson(Result.error("NOT_LOGIN"));
            response.setContentType("application/json;charset=utf-8");
            response.getWriter().write(json); //返回浏览器错误提示
            System.out.println("token = " + token);
            return  false;
        }
        try {
            JwtUtils.parseJWT(token);

        } catch (Exception e) {
            String json = gson.toJson(Result.error("NOT_LOGIN"));
            response.setContentType("application/json;charset=utf-8");
            response.getWriter().write(json);
            return  false; //禁止同行

        }
        //放行
        return true;
    }
}
