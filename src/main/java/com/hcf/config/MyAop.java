package com.hcf.config;

import com.hcf.pojo.Emp;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Aspect //标记为aop配置类
@Component //交个springBoot管理
public class MyAop {

    @Pointcut("execution(* com.hcf.controller.*.*(..))")
    public void hcf(){}

    //环绕通知
    public Object m(ProceedingJoinPoint pjp) throws Throwable {
        //拿到被切方法参数
        Object[] args = pjp.getArgs();
        //被切方法自由一个参数并且参数类型为Emp
        if (args.length == 1 && args[0] instanceof Emp){
           Emp emp= (Emp) args[0];
           emp.setUpdateTime(LocalDateTime.now());
        }
        Object object = pjp.proceed();
        return object;
    }
}
