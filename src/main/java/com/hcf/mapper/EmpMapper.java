package com.hcf.mapper;

import com.hcf.pojo.Emp;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 员工持久层
 */
@Mapper
public interface EmpMapper {

    /*   *//**
     * 查询总记录数
     * @return
     *//*
    @Select("select count(*) from emp ")
    Long count();

    *//**
     * 分页查询
     * @param start
     * @param pageSize
     * @return
     *//*
    @Select("select * from emp limit #{start},#{pageSize}")
    List<Emp> page(Integer start, Integer pageSize);

*/

    /**
     * 员工查询分页
     *
     * @return
     */
    //@Select("select * from emp")
    List<Emp> pageEmp(String name, Short gender, LocalDate begin, LocalDate end);

    /**
     * 根据id删除员工
     * @param ids
     */
    void deleteEmp(List<Integer> ids);

    /**
     * 新增员工
     * @param emp
     */
    @Insert("insert into tlias.emp (username, name, gender, image, job, entrydate, dept_id, create_time, update_time) values" +
            " (#{username},#{name},#{gender},#{image},#{job},#{entrydate},#{deptId},#{createTime},#{updateTime})")
    void insertEmp(Emp emp);

    /**
     * 根据id查询数据
     * @param id
     * @return
     */
    @Select("select * from emp where id=#{id}")
    Emp selectById(Integer id);

    /**
     * 修改员工
     * @param emp
     */
    void updateEmp(Emp emp);


    /**
     * 根据用户名和用户密码查询
     * @param emp
     * @return
     */
    @Select("select * from tlias.emp where username=#{username} and password=#{password}")
    Emp login(Emp emp);
}
