package com.hcf.mapper;

import com.hcf.pojo.Dept;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * 部门持久层
 */
@Mapper
public interface DeptMapper {
    /**
     * 查询所有部门数据
     * @return
     */
    @Select("select * from dept")
    List<Dept> findAllDept();

    /**
     * 根据id删除部门
     * @param id
     */
    @Delete("delete from dept where dept.id=#{id}")
    void deleteByIdDept(Integer id);

    /**
     * 添加部门
     * @param dept
     */
    @Insert("insert into dept (name,create_time,update_time) values (#{name},#{createTime},#{updateTime})")
    void insertDept(Dept dept);

    /**
     * 根据id查询部门回显数据
     * @param id
     * @return
     */
    @Select("select * from dept where id=#{id}")
    Dept selectById(Integer id);

    /**
     * 修改
     * @param dept
     */
    @Update("update tlias.dept set name=#{name},update_time=now() where id=#{id}")
    void updateDept(Dept dept);
}
