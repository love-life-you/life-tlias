package com.hcf.service;

import com.hcf.pojo.Dept;

import java.util.List;

/**
 * 部门业务层接口
 */
public interface DeptService {

    /**
     * 查询所有部门数据
     * @return
     */
    List<Dept> findAllDept();

    /**
     * 根据id删除部门数据
     * @param id
     */
    void deleteById(Integer id);

    /**
     * 添加部门
     * @param dept
     */
    void insertDept(Dept  dept);

    /**
     * 根据id查询部门回显数据
     * @param id
     * @return
     */
    Dept selectById(Integer id);

    void updateDept(Dept dept);
}
