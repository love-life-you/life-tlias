package com.hcf.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.hcf.mapper.EmpMapper;
import com.hcf.pojo.Emp;
import com.hcf.vo.PageBean;
import com.hcf.service.EmpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 员工业务层
 */
@Service
public class EmpServiceImpl implements EmpService {

    @Autowired
    private EmpMapper empMapper;


    /* */

    /**
     * 分页查询
     *
     * @param page
     * @param pageSize
     * @return
     *//*
    @Override
    public PageBean pageEmp(Integer page, Integer pageSize) {
        //1.获取总记录数
        Long count = empMapper.count();

        //2.获取数据列表
        Integer start=(page-1)*pageSize;
        List<Emp> page1 = empMapper.page(start, pageSize);

        //3.把结果封装到pageBean
        PageBean pageBean = new PageBean(count, page1);

        return pageBean;
    }*/
    @Override
    public PageBean pageEmp(Integer page, Integer pageSize, String name, Short gender, LocalDate begin, LocalDate end) {
        //1.设置分页参数
        PageHelper.startPage(page, pageSize);


        //2.查询列表
        List<Emp> empList = empMapper.pageEmp(name, gender, begin, end);
        Page<Emp> pageList = (Page<Emp>) empList;


        //3.把结果封装到pageBean
        PageBean pageBean = new PageBean(pageList.getTotal(), pageList.getResult());

        return pageBean;
    }

    /**
     * 根据i删除员工
     * @param ids
     */
    @Override
    public void deleteEmp(List<Integer> ids) {
        empMapper.deleteEmp(ids);

    }

    @Override
    public void insertEmp(Emp emp) {
        emp.setCreateTime(LocalDateTime.now());
        emp.setUpdateTime(LocalDateTime.now());
        empMapper.insertEmp(emp);
    }

    /**
     * 根据id查询数据
     * @param id
     * @return
     */
    @Override
    public Emp selectById(Integer id) {
        Emp emps=  empMapper.selectById(id);
        return emps;
    }

    /**
     * 修改员工
     * @param emp
     */
    @Override
    public void updateEmp(Emp emp) {
        empMapper.updateEmp(emp);

    }

    @Override
    public Emp login(Emp emp) {
        return empMapper.login(emp);
    }
}

