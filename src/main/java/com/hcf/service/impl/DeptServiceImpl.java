package com.hcf.service.impl;

import com.hcf.mapper.DeptMapper;
import com.hcf.pojo.Dept;
import com.hcf.service.DeptService;
import com.hcf.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 部门业务层
 */
@Service
public class DeptServiceImpl implements DeptService {


    @Autowired
    private DeptMapper deptMapper;


    /**
     * 查询部门数据
     * @return
     */
    @Override
    public List<Dept> findAllDept() {

      List<Dept> deptList = deptMapper.findAllDept();

      return deptList;

    }

    /**
     * 根据id删除部门
     * @param id
     */
    @Override
    public void deleteById(Integer id) {
        deptMapper.deleteByIdDept(id);

    }

    /**
     * 添加部门
     * @param dept
     */
    @Override
    public void insertDept(Dept dept) {
        dept.setCreateTime(LocalDateTime.now());
        dept.setUpdateTime(LocalDateTime.now());
        deptMapper.insertDept(dept);
    }


    /**
     * 根据id查询回显数据
     * @param id
     * @return
     */
    @Override
    public Dept selectById(Integer id) {
       Dept dept= deptMapper.selectById(id);
        return dept;
    }

    @Override
    public void updateDept(Dept dept) {
        deptMapper.updateDept(dept);
    }


}
