package com.hcf.service;

import com.hcf.pojo.Emp;
import com.hcf.vo.PageBean;

import java.time.LocalDate;
import java.util.List;

/**
 * 部门业务层接口
 */
public interface EmpService {
    /**
     * 分页查询
     * @param page 页数
     * @param pageSize 每页展示数量
     * @param name
     * @param gender
     * @param begin
     * @param end
     * @return
     */
    PageBean pageEmp(Integer page, Integer pageSize, String name, Short gender, LocalDate begin, LocalDate end);

    /**
     * 根据id删除员工
     * @param
     */
    void deleteEmp(List<Integer> ids);

    /**
     * 新增员工
     * @param emp
     */
    void insertEmp(Emp emp);

    /**
     * 根据id查询数据
     * @param id
     * @return
     */
    Emp selectById(Integer id);

    /**
     * 修改员工
     * @param emp
     */
    void updateEmp(Emp emp);

    /**
     * 登录
     * @param emp
     * @return
     */
    Emp login(Emp emp);
}
