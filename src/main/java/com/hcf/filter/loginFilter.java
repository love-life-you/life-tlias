package com.hcf.filter;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.hcf.utils.JwtUtils;
import com.hcf.vo.Result;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 过滤器 登录校验
 */
@Slf4j
//@WebFilter("/*")
public class loginFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        //强转
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse reps = (HttpServletResponse) response;

        //2.获取请求url路径
        String uri = req.getRequestURI();
        log.info("uri的值是: {}",uri);

        //3.判断请求uri是否是登录,如果是就放行
        if ("/login".equals(uri)){
            chain.doFilter(req,reps);
            return;
        }
        //4.走到这一行一定不是登录,获取请求头token(令牌)
        String token = req.getHeader("token");

        //5.token不存在 返回前端未登录 json格式返回
        if (token == null || token.equals("")){
            String json = new ObjectMapper().writeValueAsString(Result.error("NOT_LOGIN"));
            reps.setContentType("application/json;charset=utf-8");
            reps.getWriter().write(json);
            return;

        }
        //6.解析token 如果解析失败 返回前端未登录
        try{
            JwtUtils.parseJWT(token);
        }catch (Exception e) {
            //出现异常,token校验不通过.返回错误提示
            String json = new ObjectMapper().writeValueAsString(Result.error("NOT_LOGIN"));
            reps.setContentType("application/json;charset=utf-8");
            reps.getWriter().write(json);
            return;
        }

        //7.放行
        chain.doFilter(req,reps);


    }
}
