package com.itheima;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@ServletComponentScan //开启过滤器
@SpringBootApplication
public class SpringBootFilterInterceptorApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootFilterInterceptorApplication.class);
    }
}
