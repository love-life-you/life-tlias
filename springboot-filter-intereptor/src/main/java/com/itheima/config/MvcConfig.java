package com.itheima.config;


import com.itheima.interceptor.Interceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 配置类 配置拦截器
 */
@Configuration //配置类注解 代表为配置类 接口:定义规范 拥有该接口的能力\
public class MvcConfig implements WebMvcConfigurer {

    @Autowired
    private Interceptor interceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //添加拦截器
        registry.addInterceptor(interceptor)
                //设置拦截路径
                .addPathPatterns("/**")
                // 设置排除路径
                .excludePathPatterns("/a/c");

    }
}
