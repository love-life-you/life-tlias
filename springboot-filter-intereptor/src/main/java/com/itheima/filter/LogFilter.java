package com.itheima.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;


//@WebFilter("/*") //拦截所有请求
public class LogFilter implements Filter {



    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        System.out.println("请求到达过滤器,访问资源之前");

        //拦截通过放行
        chain.doFilter(request,response);

        System.out.println("请求到达过滤器,访问资源之后");
    }



}
