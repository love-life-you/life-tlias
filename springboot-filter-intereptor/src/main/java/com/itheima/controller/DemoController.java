package com.itheima.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {

    @GetMapping("/a")
    public String a() {
        System.out.println("a方法");
        return "a方法";
    }

    @GetMapping("/a/b")
    public String ab() {
        System.out.println("ab方法");
        return "a/b方法";
    }

    @GetMapping("/a/c")
    public String ac() {
        System.out.println("ac方法");
        return "a/c方法";
    }
}
